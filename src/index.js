angular
  .module('app', [
    'ui.router'
  ])
  .config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('first',{
            url: '/',
            templateUrl: 'templates/first.html',
            controller: 'firstCtrl'
        })
        .state('main', {
          url: '/main',
          templateUrl: 'templates/main.html',
          controller:'mainCtrl',
          resolve: {
              friends : ['$http', function($http){
                  return $http.get('/api/friends.json').then(function(respons){
                      console.log(respons.data);
                      return respons.data;
                  });
              }]
          }
        }).
        state('roles',{
            url: '/roles',
            templateUrl: 'templates/roles.html',
            controller: 'rolesCtrl'
        }).
        state('begin',{
            url: '/',
            templateUrl: 'templates/first.html',
            controller: 'firstCtrl'
        });
  }]);
