angular
    .module('app')
    .controller('mainCtrl', ['$scope', 'friends', '$http' , function($scope, friends, $http){
        $scope.title = "Users";
        $scope.friends= friends;
        $scope.sortType     = 'name'; // значение сортировки по умолчанию
        $scope.sortReverse  = false;
        $scope.editing = false;

        $scope.addNewRow = function(){
            $scope.friends.push({name: "Edit", age:"Edit", phone:"Edit"})
        };
        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.save = function(){
            $http.post('/api/friends.json', friends, config).then(function(){
                alert("New users data is saved");
            });
        }
        // $scope.save = function(){
        //     $http.post('/api/friends', friends)
        // }
    }]);
